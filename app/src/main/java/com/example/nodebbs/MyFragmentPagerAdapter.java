package com.example.nodebbs;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private final int PAGER_COUNT = 4;
    private MyFragment1 myFragment1 = null;
    private MyFragment2 myFragment2 = null;
    private MyFragment3 myFragment3 = null;
    private MyFragment4 myFragment4 = null;

    public MyFragmentPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
        myFragment1 = new MyFragment1();
        myFragment2 = new MyFragment2();
        myFragment3 = new MyFragment3();
        myFragment4 = new MyFragment4();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case MainActivity.PAGE_ONE:
                fragment = myFragment1;
                Log.d("tab", "getItem: 1");
                break;
            case MainActivity.PAGE_TWO:
                fragment = myFragment2;
                Log.d("tab", "getItem: 2");
                break;
            case MainActivity.PAGE_THREE:
                fragment = myFragment3;
                Log.d("tab", "getItem: 3");
                break;
            case MainActivity.PAGE_FOUR:
                fragment = myFragment4;
                Log.d("tab", "getItem: 4");
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGER_COUNT;
    }
}